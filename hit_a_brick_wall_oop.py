#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, pygame

width, height, bg_color = 800, 700, (80, 60, 0)
white, black, magenta, green, yellow = (255, 255, 255), (0, 0, 0), (200, 0, 220), (0, 255, 0), (255, 255, 100)
brick_wall, brick_rows = [], 8
bg_image = pygame.image.load("Pic/bg.jpg")
ball_init_speed = paddle_init_speed = None
ticks = 80
admin, press, gunfire = 0, 0, 0
score, life, fire_mag = 0, 4, 3

class Brickboost:
    def __init__(self, ball_speed=(0, 8), paddle_speed=25, lives=3, score=0):

        self.ball_init_speed = ball_speed
        self.paddle_init_speed = paddle_speed
        self.fire_init_speed = (0, 0)

        self.screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption("Project 2: Hit A Brick Wall")

        pygame.mixer.pre_init(44100, -16, 1, 512)
        pygame.init()
        pygame.key.set_repeat(1, 30)
        pygame.mouse.set_visible(255)

        self.life = lives
        self.score = score
        self.level, self.check = 1, 1

        self.hit_paddle_sound = pygame.mixer.Sound('Sound/hitDown.wav')
        self.hit_brick_sound = pygame.mixer.Sound('Sound/hitTop.wav')
        self.hit_top_sound = pygame.mixer.Sound('Sound/hitUp.wav')
        self.end_sound = pygame.mixer.Sound('Sound/theEnd.wav')
        self.fire_sound = pygame.mixer.Sound('Sound/fire.wav')

        self.paddle_surface = pygame.image.load("Pic/paddle.png")
        self.ball_surface = pygame.image.load("Pic/ball.png")
        self.brick_surface = pygame.image.load("Pic/brick.png")
        self.fire_surface = pygame.image.load("Pic/fireball.png").convert_alpha()
        self.gun_surface = pygame.image.load("Pic/gun.png").convert_alpha()
        self.life_surface = pygame.image.load("Pic/soul.png").convert_alpha()
        
        self.paddle = self.paddle_surface.get_rect()
        self.ball = self.ball_surface.get_rect()
        self.brick = self.brick_surface.get_rect()
        self.fire = self.fire_surface.get_rect()
        self.gun = self.gun_surface.get_rect()

        self.paddle.move_ip((width/2 - self.paddle.width/2)-30, height - 20)
        self.ball.move_ip((width/2 - self.ball.width/2)-30, height-47)
        self.gun.move_ip(self.paddle.left-20, self.paddle.top-10)
        self.fire.move_ip((width/2 - self.ball.width/2)-30, height - 20)

        self.clock = pygame.time.Clock()

        self.create_bricks(self.level)

    def create_bricks(self, level):
        y = 80
        bw, bh = self.brick.size

        brick_wall.clear()
        space = 40
        if level == 1:          # Brick for level 1
            for j in range(8):
                for i in range(10):
                    brick_wall.append(self.brick.move(space+(i%13)*bw,y))
                    space += 20
                space = 40
                y += bh+20

    def quit():
        """Exit the program."""
        pygame.quit()
        sys.exit()

    def event_handler(self, event):
        if event.type == pygame.QUIT:  # e.g. Close a game window
            quit()
        elif event.type == pygame.KEYDOWN:
            if event.key in [pygame.K_ESCAPE, ord('q'), ord('Q')]:
                quit()
            elif event.key == pygame.K_LEFT:
                self.paddle.move_ip(-self.move_paddle, 0)
                press = 1
                if self.check == 1:
                    self.check = 2
                if self.paddle.left < 0:
                    self.paddle.left = 0
            if event.key == pygame.K_RIGHT:
                self.paddle.move_ip(self.move_paddle, 0)
                press = 1
                if self.check == 1:
                    self.check = 3
                if self.paddle.right > width:
                    self.paddle.right = width
            if event.key == pygame.K_UP and self.check == 1:
                self.check = 4
                press = 1
            if event.key == pygame.K_RETURN:
                self.check = 1
                press = 1
            if event.key == pygame.K_y:
                self.check = 1
                Brickboost().play()
            if event.key == pygame.K_LCTRL and fire_mag :
                gunfire = 1
                press = 1
                self.fire_sound.play(0)

            if event.key == pygame.K_l:
                press = 1
            if event.key == pygame.K_o and press == 1:
                press = 2
            if event.key == pygame.K_p and press == 2:
                admin = 2

    def texts(self, text_display, text_locate, text_color, size, style):
        font = pygame.font.Font(style, size)
        scoreText = font.render(text_display, True, text_color)
        self.screen.blit(scoreText, text_locate)

    def blink_text(self, text=""):
        """Display a blinking text in the middle of the window, while waiting for
        users to exit the program.
        """
        buffer = pygame.Surface(pygame.display.get_surface().get_size())
        buffer.blit(pygame.display.get_surface(), (0,0))

        font = pygame.font.Font(None,70)
        text_surface = font.render(text, True, (0,255,0), bg_color)
        surface = pygame.Surface(font.size(text))
        surface.blit(text_surface, (0, 0))
        text_rect = text_surface.get_rect()
        text_rect.move_ip(width/2 - text_rect.centerx, 0.4*height)
        dir, alpha = -5, 255
        while True:
            self.clock.tick(ticks)
            for event in pygame.event.get():
                self.event_handler(event)
            pygame.display.get_surface().blit(buffer,(0,0))
            surface.set_alpha(alpha)
            self.screen.blit(surface, text_rect)
            pygame.display.flip()
            alpha += dir
            if alpha in [0, 255]:
                dir = -dir

    def play(self):
        global admin, gunfire, fire_mag
        self.create_bricks
        move_ball_x, move_ball_y = self.ball_init_speed
        move_fire_x, move_fire_y = self.fire_init_speed
        self.move_paddle = self.paddle_init_speed
        clock = pygame.time.Clock()

        while True:
            clock.tick(ticks)
            for event in pygame.event.get():
                self.event_handler(event)

            if admin == 2:
                self.life, fire_mag = 9999, 9999
                press = 0
                admin = 0
            if self.check == 1:
                move_ball_x, move_ball_y = (0, 0)
                self.check = 1
            elif self.check == 2:
                if self.level == 3:
                    move_ball_x, move_ball_y = (-5, -9)
                else :
                    move_ball_x, move_ball_y = (-5, -8)
                self.check = -1
            elif self.check == 3:
                if self.level == 3:
                    move_ball_x, move_ball_y = (5, -9)
                else :
                    move_ball_x, move_ball_y = (5, -7)
                self.check = -1
            elif self.check == 4:
                if self.level == 3:
                    move_ball_x, move_ball_y = (0, -9)
                else :
                    move_ball_x, move_ball_y = (0, -7)
                self.check = -1
            
            if gunfire == 1:
                #self.fire = self.fire_surface.get_rect()
                #self.fire.move_ip(self.paddle.left, self.paddle.top)
                move_fire_x, move_fire_y = (0,-25)
                gunfire = -1
                fire_mag -= 1

            if self.paddle.bottom-move_ball_y >= self.ball.bottom >= self.paddle.top and \
                self.paddle.right >= self.ball.centerx >= self.paddle.left:  # Ball hits the paddle
                move_ball_y = -move_ball_y  # Bounce the ball back

                if move_ball_x != 0:
                    move_ball_x = 0
                if self.ball.centerx < self.paddle.left+60:
                    if self.ball.centerx > self.paddle.left+45:
                        move_ball_x = move_ball_x - 2
                    elif self.ball.centerx > self.paddle.left+30:
                        move_ball_x = move_ball_x - 3
                    elif self.ball.centerx > self.paddle.left+13:
                        move_ball_x = move_ball_x - 5
                    else :
                        move_ball_x = move_ball_x - 2
                elif self.ball.centerx >= self.paddle.right-60:
                    if self.ball.centerx < self.paddle.right-45:
                        move_ball_x = move_ball_x + 2
                    elif self.ball.centerx < self.paddle.right-30:
                        move_ball_x = move_ball_x + 3
                    elif self.ball.centerx < self.paddle.right-13:
                        move_ball_x = move_ball_x + 5
                    else :
                        move_ball_x = move_ball_x + 2
                elif self.ball.centerx > self.paddle.left+60 and self.ball.centerx < self.paddle.right-60:
                    move_ball_x = 0

                self.hit_paddle_sound.play(0)

            self.ball.move_ip(move_ball_x, move_ball_y)    # Move ball to new position
            if gunfire == -1:
                self.fire.move_ip(move_fire_x, move_fire_y)
            if self.ball.top > height or len(brick_wall) == 0:
                if self.life :
                    self.paddle = self.paddle_surface.get_rect()
                    self.ball = self.ball_surface.get_rect()
                    self.paddle.move_ip((width/2 - self.paddle.width/2)-30, height - 20)
                    self.ball.move_ip((width/2 - self.ball.width/2)-30, height-47)
                    move_ball_x, move_ball_y = (0, 8)
                    self.check = 1
                    self.life -= 1
                else :
                    self.end_sound.play(0)
                    self.blink_text("Game Over. (Play again? y/n")

            if self.ball.top < 0:    # The ball hits the upper side of the window
                move_ball_y = -move_ball_y
                self.hit_top_sound.play(0)

            if self.ball.left <= 0:
                if move_ball_x <= 0:
                    move_ball_x = -move_ball_x
                move_ball_x = move_ball_x
                self.hit_top_sound.play(0)

            if self.ball.right >= 799:
                move_ball_x = -move_ball_x
                self.hit_top_sound.play(0)


            index = self.ball.collidelist(brick_wall)  # Find a brick that the ball hits
            indexfire = self.fire.collidelist(brick_wall)
            if index >= 0:
                #move_ball_y = -move_ball_y    # Bounce the ball back after hit
                if self.ball.right >= brick_wall[int(index)].left and self.ball.centerx + 5 < brick_wall[int(index)].left:
                    move_ball_x = -move_ball_x
                elif self.ball.left <= brick_wall[int(index)].right and self.ball.centerx - 5 > brick_wall[int(index)].right:
                    move_ball_x = -move_ball_x
                else :
                    move_ball_y = -move_ball_y
                self.hit_brick_sound.play(0)
                brick_wall.pop(index)
                self.score += 10
            if indexfire >= 0:
                if self.fire.top <= brick_wall[int(indexfire)].bottom :
                    gunfire = 0
                    move_fire_y = -move_fire_y
                    brick_wall.pop(indexfire)
                    score += 10
                self.hit_brick_sound.play(0)
            
            self.screen.fill(bg_color)                 # Clear the screen
            self.screen.blit(bg_image, (0, 0))           # Set background
            self.screen.blit(self.life_surface, (0,664))      # Display Life surface

            for b in brick_wall:                  # Display the remaining bricks
                self.screen.blit(self.brick_surface, b)
            if gunfire == -1:                       # Display fire
                self.screen.blit(self.fire_surface, self.fire)
            elif fire_mag == 0:                     
                self.fire.move_ip(1000,1000)
            else :                                  # Display gun
                self.screen.blit(self.gun_surface, self.paddle)
            self.screen.blit(self.ball_surface, self.ball)       # Display the ball
            self.screen.blit(self.paddle_surface, self.paddle)   # Display the paddle
            self.texts("     X %d"%(self.life), (0,680), white, 30, None) # Display life
            self.texts("     Life", (0,660), white, 30, None) # Display Life
            self.texts("Score:: %s"%(str(self.score)), (655, 10), magenta, 30, "Font/TALKINGTOTHEMOON.TTF")  # Display score
            self.texts("Level:: %s"%(str(self.level)), (350, 10), green, 35, "Font/TALKINGTOTHEMOON.TTF")    # Display level
            self.texts("Ammo:: %s"%(str(fire_mag)), (17, 10), magenta, 30, "Font/TALKINGTOTHEMOON.TTF") # Display Gun ammo
            pygame.display.flip()

if __name__ == '__main__':
    Brickboost().play()
